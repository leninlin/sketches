#include <Wire.h>
#include <I2C.h>

#define MASTER_ADDRESS 0x03

void setup()
{
    i2c_begin(MASTER_ADDRESS);
    Serial.begin(9600);

    int pir = i2c_addDevice(1, "PIR Motion");
    i2c_onGetState(pir, getPIRState);

    int led = i2c_addDevice(1, "White LED");
    i2c_onGetState(led, getLEDState);
    i2c_onSetState(led, setLEDState);
}

void loop()
{

}

int getPIRState()
{
    if(digitalRead(2) == HIGH) {
        return 1;
    } else {
        return 0;
    }
}

int getLEDState()
{
    return digitalRead(3);
}

void setLEDState(int data[])
{
    if(data[0] == 0x01) {
        digitalWrite(3, HIGH);
    } else {
        digitalWrite(3, LOW);
    }

    return;
}