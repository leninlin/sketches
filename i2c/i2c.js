exec = require('child_process').exec;
i2c = require('i2c');

addressInfo = {};
addresses = [];

scanAddresses = function(callback) {
    exec('i2cdetect -y 1', function(err, stdout, stderr) {
        var scanedAddresses = [];
        var lines = stdout.split('\n');
        lines.shift();
        for (i in lines) {
            var line = lines[i].split(' ');
            line.shift();
            for (k in line) {
                if (line[k] != '' && line[k] != '--') {
                    scanedAddresses.push(parseInt(line[k], 16));
                }
            }
        }
        addresses = scanedAddresses;

        if (typeof callback == 'function') {
            callback();
        }
    });
}

getAddressInfo = function(address) {
    if (!addressInfo[address]) {
        addressInfo[address] = {};
    }

    if (!addressInfo[address].conn) {
        addressInfo[address].conn = new i2c(address, {device: '/dev/i2c-1'});
    }

    // get count devices on address
    addressInfo[address].conn.readBytes(1, 1, function(err, res) {
        if (res[0] > 0) {
            addressInfo[address].countDevices = res[0];

            addressInfo[address].devices = addressInfo[address].devices || {};

            for (i = 0; i < res[0]; i++) {
                getDeviceType(address, i);
                getDeviceName(address, i);
                getDeviceState(address, i);
            }
        }
    });
}

getDeviceType = function(address, device) {
    if (addressInfo[address] && addressInfo[address].conn) {
        addressInfo[address].conn.writeBytes(2, [device], function(err) {
            addressInfo[address].conn.readBytes(2, 1, function(err, res) {
                if (!err) {
                    addressInfo[address].devices[device] = addressInfo[address].devices[device] || {};
                    addressInfo[address].devices[device].type = res[0];
                }
            });
        });
    }
}

getDeviceName = function(address, device) {
    if (addressInfo[address] && addressInfo[address].conn) {
        addressInfo[address].conn.writeBytes(3, [device], function(err) {
            addressInfo[address].conn.readBytes(3, 20, function(err, res) {
                if (!err) {
                    addressInfo[address].devices[device] = addressInfo[address].devices[device] || {};
                    addressInfo[address].devices[device].name = '';
                    for (k = 0; k < 20; k++) {
                        if (res[k] != '00') {
                            addressInfo[address].devices[device].name += String.fromCharCode(res[k]);
                        }
                    }
                }
            });
        });
    }
}

getDeviceState = function(address, device) {
    if (addressInfo[address] && addressInfo[address].conn) {
        addressInfo[address].conn.writeBytes(4, [device], function(err) {
            addressInfo[address].conn.readBytes(4, 1, function(err, res) {
                if (!err) {
                    addressInfo[address].devices[device] = addressInfo[address].devices[device] || {};
                    addressInfo[address].devices[device].status = {value: res[0], time: new Date().getTime()};
                    console.log(addressInfo[address].devices[device]);
                }
            });
        });
    }
}

setDeviceState = function(address, device, data) {
    if (addressInfo[address] && addressInfo[address].conn) {
        data.unshift(device);
        addressInfo[address].conn.writeBytes(5, data, function(err) {
            getDeviceState(address, device);
        });
    }
}

scanAddresses(function() {
    for (i in addresses) {
        getAddressInfo(addresses[i]);
    }
});
