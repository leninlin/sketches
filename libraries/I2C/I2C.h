#ifndef I2C_h
#define I2C_h

#define MAX_DEVICES 10
#define MAX_RECEIVED_BYTES 10
#define LENGTH_DEVICE_NAME 20 // for all addresses connected to RPi

// device info fields
// 1 - type
// 2 - name
// 3 - getState function
// 4 - setState function
// ...

// device types
// 1 - bool sensor (device return 1 or 0)
// 2 - int sensor (device return int value. for example, temperature sensor)
// ...

// commands
// 1 - get count devices
// 2 - get 'type' field for device N
// 3 - get 'name' field for device N
// 4 - get state sfor device N
// 5 - set state for device N
// ...

struct Device {
    int type;
    char name[LENGTH_DEVICE_NAME];
    int (*getState)();
    void (*setState)(int[]);
};

void i2c_begin(int);

int i2c_addDevice(int, char[]);

int getCountDevices();
int getTypeDevice(int);

void i2c_onGetState(int, int (*)());
void i2c_onSetState(int, void (*)(int[]));

void receiveEvent(int);
void requestEvent();

#endif
