#include <Wire.h>
#include <I2C.h>

int master_address;

int countDevices = 0;
Device devices[MAX_DEVICES];

int receiveCommand = 0;
int receiveDeviceN = 0;
int receiveData[MAX_RECEIVED_BYTES];

void i2c_begin(int address)
{
    master_address = address;
    Wire.begin(master_address);

    Wire.onReceive(receiveEvent);
    Wire.onRequest(requestEvent);
}

void receiveEvent(int bytes)
{
    int receiveNewCommand = 0;
    receiveNewCommand = Wire.read();

    switch (receiveNewCommand) {
        case 1: // get count devices
            receiveCommand = receiveNewCommand;
            break;
        case 2: // ... for device N
        case 3:
        case 4:
            if (receiveCommand == receiveNewCommand) {
                break;
            }
            receiveDeviceN = Wire.read();
            break;
    }
    receiveCommand = receiveNewCommand;
    int i = 0;

    while (Wire.available()) {
        receiveData[i] = Wire.read();
        i++;
    }

    if (i < sizeof(receiveData)) {
        for (int k = i; k < sizeof(receiveData); k++) {
            receiveData[k] = 0;
        }
    }

    switch (receiveCommand) {
        case 4: // set state for device N
            if (devices[receiveDeviceN].setState) {
                devices[receiveDeviceN].setState(receiveData);
            }

            break;
    }

}

void requestEvent()
{
    if (receiveCommand > 0) {
        switch (receiveCommand) {
            case 1:
                Wire.write(getCountDevices());
                break;
            case 2:
                Wire.write(getTypeDevice(receiveDeviceN));
                break;
            case 3:
                uint8_t data[20] = {};
                int i = 0;
                while (i < 20) {
                    data[i] = devices[receiveDeviceN].name[i];
                    i++;
                }
                Wire.write(data, 20);
                break;
        }
    }
}

int getCountDevices()
{
    return countDevices;
}

int getTypeDevice(int device)
{
    return devices[device].type;
}

// sets function called on `getState` action for device `deviceN`
void i2c_onGetState(int deviceN, int (*function)())
{
    if (devices[deviceN].name) {
        devices[deviceN].getState = function;
    }
}

// sets function called on `setState` action for device `deviceN`
void i2c_onSetState(int deviceN, void (*function)(int[]))
{
    if (devices[deviceN].name) {
        devices[deviceN].setState = function;
    }
}

// sets function called on `setState` action for device `deviceN`
int i2c_addDevice(int type, char name[LENGTH_DEVICE_NAME])
{
    devices[countDevices].type = type;
    strncpy(devices[countDevices].name, name, LENGTH_DEVICE_NAME);

    countDevices++;

    return countDevices-1;
}