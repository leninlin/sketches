#define PN532_P2P_DEBUG 1;
#define PN532DEBUG 1;

#include <Wire.h>
#include <nfc.h>

NFC_Module nfc;

void setup(void) 
{
    Serial.begin(9600);
    nfc.begin();
    Serial.println("MF1S50 Reader Demo From Elechouse.");

    uint8_t versiondata = nfc.get_version();

    if (!versiondata) {
        Serial.println("Didn't find PN53x board.");
        while (1);
    }

    Serial.print("Found chip PN5");
    Serial.println((versiondata>>24) & 0xFF, HEX);
    Serial.print("Firmware ver. ");
    Serial.print((versiondata>>16) & 0xFF, DEC);
    Serial.print(".");
    Serial.println((versiondata>>8) & 0xFF, DEC);

    nfc.SAMConfiguration();
}

void loop(void)
{
    u8 buf[32], sta;

    sta = nfc.InListPassiveTarget(buf);

    if (sta && buf[0] == 4) {
        Serial.print("UUID length:");
        Serial.print(buf[0], DEC);
        Serial.println();
        Serial.print("UUID:");

        nfc.puthex(buf+1, buf[0]);

        Serial.println();

        u8 key[6] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
        u8 key2[6] = {0x2D, 0x20, 0x4E, 0x46, 0x43, 0x20};
        u8 key3[6] = {0x45, 0x6C, 0x65, 0x63, 0x68, 0x6F};
        u8 blockKey[16] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x75, 0x73, 0x65, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
        u8 blocknum = 1;
        u8 block[16];

        for(int i=0; i<=32; i+=4){
            
            if (i == 4) {
                sta = nfc.MifareAuthentication(0, i, buf+1, buf[0], key3);

                if (sta) {
        //            u8 block[16];
                    Serial.print("Authentication success: ");
                    Serial.println(i);

/*                sta = nfc.MifareReadBlock(i+3, block, 6);
                if (sta) {
                    nfc.puthex(block, 16);
                    Serial.write(block, 16);
                    Serial.println();
                }*/
                    //write
//                    strcpy((char*)block, "Elechouse - NFC Elechouse - NFC");
                     sta = nfc.MifareWriteBlock(i+3, blockKey);
                     if (sta) {
                         Serial.println("Write block successfully.");
                     }
                }


                //continue;
            }

            sta = nfc.MifareAuthentication(1, i, buf+1, buf[0], key);
            if (sta) {
    //            u8 block[16];
                Serial.print("Authentication success: ");
                Serial.println(i);

                if (i == 4) {
                    //write
                    // strcpy((char*)block, "Elechouse - NFC Elechouse - NFC");
                    // sta = nfc.MifareWriteBlock(i+3, block);
                    // if (sta) {
                    //     Serial.println("Write block successfully.");
                    // }
                }

                //read block 4
                sta = nfc.MifareReadBlock(i, block);
                if (sta) {
                    nfc.puthex(block, 16);
                    Serial.write(block, 16);
                    Serial.println();
                }

                //read block 5
                sta = nfc.MifareReadBlock(i+1, block);
                if (sta) {
                    nfc.puthex(block, 16);
                    Serial.write(block, 16);
                    Serial.println();
                }

                //read block 6
                sta = nfc.MifareReadBlock(i+2, block);
                if (sta) {
                    nfc.puthex(block, 17);
                    Serial.write(block, 16);
                    Serial.println();
                }

                //read block 7
                sta = nfc.MifareReadBlock(i+3, block);
                if (sta) {
                    nfc.puthex(block, 16);
                    Serial.write(block, 16);
                    Serial.println();
                }
            }
        }
    }
}